#!/bin/bash



for((;;))
do
	#Temperature vars
	cpuTemp0=$(cat /sys/class/thermal/thermal_zone0/temp)
	cpuTemp1=$((cpuTemp0/1000))
	cpuTemp2=$(($cpuTemp0/100))
	cpuTempM=$(($cpuTemp2 % $cpuTemp1))
	tempString="$cpuTemp1.$cpuTempM"
	#Commands vars
	topCommand=$(top -b -n 2 | grep Cpu | tail -1)
	freeCommand=$(free | grep Mem)
	echo "$topCommand" >> log.txt
	echo "$freeCommand" >> log.txt
	echo "CpuTemp: $tempString �C">>log.txt
	echo "$tempString"
	echo "$topCommand"
	echo "$freeCommand"
done

