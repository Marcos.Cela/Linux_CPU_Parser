package smm.test.chart;

import java.awt.GridLayout;
import java.util.ArrayList;

import javax.swing.JFrame;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

import smm.parser.Stat;

public class MainFrame extends JFrame {

	private static final long serialVersionUID = 1L;
	public MainFrame(ArrayList<Stat> stats) {
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setSize(500, 500);
		setLayout(new GridLayout(3, 1));
		add(cpuChart(stats));
		add(tempChart(stats));
		add(memoryChart(stats));
		setVisible(true);
	}

	private static ChartPanel cpuChart(ArrayList<Stat> stats) {

		// collection = dataset
		final JFreeChart chart = ChartFactory.createXYLineChart("CPU stats", "Time", "CPU % use", cpuDataset(stats),
				PlotOrientation.VERTICAL, true, true, false);
		ChartPanel chartPanel = new ChartPanel(chart);
		return chartPanel;
	}
	private static ChartPanel tempChart(ArrayList<Stat> stats) {

		// collection = dataset
		final JFreeChart chart = ChartFactory.createXYLineChart("CPU temperature", "Time", "CPU temp �C",
				tempDataset(stats), PlotOrientation.VERTICAL, true, true, false);
		ChartPanel chartPanel = new ChartPanel(chart);
		return chartPanel;
	}
	private static ChartPanel memoryChart(ArrayList<Stat> stats) {

		// collection = dataset
		final JFreeChart chart = ChartFactory.createXYLineChart("Used memory", "Time", "Used memory", memDataset(stats),
				PlotOrientation.VERTICAL, true, true, false);
		ChartPanel chartPanel = new ChartPanel(chart);
		return chartPanel;
	}

	private static XYDataset cpuDataset(ArrayList<Stat> stats) {
		final XYSeries cpuDataset = new XYSeries("CPU use");
		// Populate dataset with money
		for (int i = 0; i < stats.size(); i++) {
			cpuDataset.add(i, stats.get(i).cpu);
		}
		final XYSeriesCollection collection = new XYSeriesCollection();
		collection.addSeries(cpuDataset);
		return collection;
	}
	private static XYDataset tempDataset(ArrayList<Stat> stats) {
		final XYSeries cpuDataset = new XYSeries("temperature �C");
		// Populate dataset with money
		for (int i = 0; i < stats.size(); i++) {
			cpuDataset.add(i, stats.get(i).temperature);
		}
		final XYSeriesCollection collection = new XYSeriesCollection();
		collection.addSeries(cpuDataset);
		return collection;
	}
	private static XYDataset memDataset(ArrayList<Stat> stats) {
		final XYSeries cpuDataset = new XYSeries("Free memory");
		// Populate dataset with money
		for (int i = 0; i < stats.size(); i++) {
			cpuDataset.add(i, stats.get(i).freeMemory);
		}
		final XYSeriesCollection collection = new XYSeriesCollection();
		collection.addSeries(cpuDataset);
		return collection;
	}
}
