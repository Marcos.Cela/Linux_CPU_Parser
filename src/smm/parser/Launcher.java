package smm.parser;

import java.io.File;
import java.util.ArrayList;
import java.util.Scanner;

import smm.test.chart.MainFrame;

public class Launcher {

	public static void main(String[] args) {
		// Parse the file that contains all the log data
		File file = new File(".");
		System.out.println("Select a file to parse:");

		File[] files = file.listFiles();
		int selected = 0;
		for (int i = 0; i < files.length; i++) {
			System.out.println(i + "\t" + files[i].getName());
		}

		Scanner input = new Scanner(System.in);
		selected = input.nextInt();
		file = new File(files[selected].getName());
		System.out.println("Parsing: " + file.getAbsolutePath());
		input.close();
		ArrayList<Stat> stats = new ArrayList<Stat>();
		Scanner scan = null;
		try {
			scan = new Scanner(file);
			while (scan.hasNextLine()) {
				// Scan lines with format
				String line = scan.nextLine();
				String parsed;
				float cpu = -1;
				float freeMem = -1;
				float temp = -1;

				parsed = line.substring(line.indexOf(" ") + 1, line.indexOf(" ") + 5);
				cpu = Float.parseFloat(parsed);

				line = scan.nextLine();

				parsed = line.substring(20, 34);
				freeMem = Float.parseFloat(parsed);

				line = scan.nextLine();

				parsed = line.substring(line.indexOf(" "), line.indexOf(".") + 2);
				temp = Float.parseFloat(parsed);

				stats.add(new Stat(cpu, temp, freeMem));

			}
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Error while parsing, check your file format, and that your file exists.");
			System.exit(1);
		}
		MainFrame frame = new MainFrame(stats);
		scan.close();

	}

}
