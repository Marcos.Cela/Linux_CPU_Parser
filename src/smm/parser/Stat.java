package smm.parser;

public class Stat {
	public float cpu;
	public float temperature;
	public float freeMemory;

	/**
	 *
	 * @param cpu
	 * @param temperature
	 * @param freeMemory
	 */
	public Stat(float cpu, float temperature, float freeMemory) {
		this.cpu = cpu;
		this.temperature = temperature;
		this.freeMemory = freeMemory;
	}
	@Override
	public String toString() {
		return "CPU: " + cpu + " MEM: " + freeMemory + " TEMP: " + temperature;
	}
}
